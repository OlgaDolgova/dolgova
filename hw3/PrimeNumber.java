package hw3;

import java.util.Scanner;

public class PrimeNumber {
    public static void main(String[] args) {
        String str = "";
        Scanner sc;
        boolean prime = true;
        boolean t = true;
        int num = 0;

        System.out.println("Введите число:");
        while (t) {
            sc = new Scanner(System.in);
            str = sc.next();

            try {
                num = Integer.parseInt(str);
                t = false;
            } catch (NumberFormatException e) {
                System.err.println("Неверный формат строки, введите число");
                t = true;
            }
        }

        System.out.println("Простые числа: ");
        System.out.println("1");
        System.out.println("2");

        for (int i = 3; i <= num; i += 2) {
            int div = 0;
            for (int j = 1; j <= i; j++) {
                if (i % j == 0)
                    div++;
                if (div > 2)break;

            }

            if (div == 2) {
                System.out.println(i);
            }
        }
    }
}



