package hw3;

public class OddNumber {
    public static void main(String[] args) {
        for (int i = 0; i <100; i++)
            if ((i%2) != 0)
                System.out.print (i + " ");

        System.out.println ();

        // with while
        int i1 = 1;
        while (i1 < 100) {
            System.out.print(i1 + " ");
            i1 = i1 + 2;
        }
    }

}