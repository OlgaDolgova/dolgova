package hw3;
import java.util.*;

public class Sum {
    public static void main(String[] args) {
        String i = "";
        double sum = 0;
        double x = 0;

        System.out.println("Enter a number or 'sum'");

        while (true) {
            Scanner sc = new Scanner(System.in);
            i = sc.next();
            if (i.equals("sum")) break;
            try {
                x = Double.parseDouble(i);
            } catch (NumberFormatException e) {
                System.err.println("Please enter number or 'sum'");
            }
            sum += x;
        }

        System.out.println("Sum = " + sum);
    }
}