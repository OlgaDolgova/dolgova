package hw3;
import java.lang.Math;

public class MinMaxValue {
    public static void main(String args[]) {
        int a[] = new int[4];
        int max_index = 0;
        int min_index = 0;
        int max = -30;
        int min = 30;
        System.out.println("Generated array consists of 4 elements (range: [-150;150])");
        for (int i = 0; i < a.length; i++) {
            a[i] = ((int) (Math.random() * 300) - 150);
            System.out.printf("%3s\n", a[i]);
            if (i > 0 && a[i] >= max) {
                max_index = i;
                max = a[i];
            }
            if (i > 0 && a[i] <= min){
                min_index = i;
                min = a[i];
            }
        }
        System.out.println("Max value is " + max + "; № of index is " + max_index);
        System.out.println("Min value is " + min + "; № of index is " + min_index);
    }
}
