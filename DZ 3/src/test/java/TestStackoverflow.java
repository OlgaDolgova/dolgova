import Stackoverflow.MainPage;
import Stackoverflow.Questions;
import Stackoverflow.SignUp;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;

import java.util.concurrent.TimeUnit;


public class TestStackoverflow {
    static WebDriver driver;
    private static String baseURL = "https://stackoverflow.com/";
    protected MainPage mainPage;
    protected SignUp signUp;
    protected Questions questions;

    @BeforeClass
    public static void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.get(baseURL);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @Before
    public void setUpBefore() throws Exception {
        if (driver == null) {
            setUp();
        } else {
            if (!driver.getCurrentUrl().equals(baseURL)) {
                driver.get(baseURL);
            }
        }
        if (mainPage == null) {
            mainPage = new MainPage(driver);
        }
    }

    @AfterClass
    public static void afterMethod() throws Exception {
        driver.close();
    }

    @Test
    public void testNumberOfFeatured() throws Exception {
        mainPage.CheckSum();
    }

    @Test
    public void testSingUp() throws Exception {
        signUp = mainPage.clickOnSingUpLnk();
        assertNotNull("'Google' button is absent on login page", signUp.btn_google);
        assertNotNull("'Facebook' button is absent on login page", signUp.btn_facebook);
    }

    @Test
    public void testQuestionDate() throws Exception {
        questions = mainPage.clickOnQuestionsLnk();
        assertTrue("Questions was asked NOT today", questions.asked_day.getText().equals("today"));
    }
}
