import Rozetka.Cart;
import Rozetka.CityDropdown;
import Rozetka.MainPage;
import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

public class TestRozetka {
    static WebDriver driver;
    private static String baseURL = "http://rozetka.com.ua/";
    protected MainPage mainPage;
    protected CityDropdown cityDropdown;
    protected Cart cart;


    @BeforeClass
    public static void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.get(baseURL);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @Before
    public void setUpBefore() throws Exception {
        if (driver == null) {
            setUp();
        } else {
            if (!driver.getCurrentUrl().equals(baseURL)) {
                driver.get(baseURL);
            }
        }
        if (mainPage == null) {
            mainPage = new MainPage(driver);
        }
    }

    @AfterClass
    public static void afterMethod() throws Exception {
        driver.close();
    }

    @Test
    public void testMainIcon() throws Exception {
        assertTrue("The logo is absent", mainPage.img_mainpage_logo.isDisplayed());
    }

    @Test
    public void testRightMenu() throws Exception {
        assertTrue("'Apple' submenu is absent", mainPage.btn_mainpage_apple.isDisplayed());
    }

    @Test
    public void testRightSubMenu() throws Exception {
        assertTrue("'MP3' section is absent", mainPage.btn_mainpage_MP3section.isDisplayed());
    }

    @Test
    public void testCitiesDropDown() throws Exception {
        cityDropdown = mainPage.clickOnCityDropdown();
        assertTrue("'Kharkiv' is not found", cityDropdown.lnk_city_kharkov.isDisplayed());
        assertTrue("'Kiev' is not found", cityDropdown.lnk_city_kiev.isDisplayed());
        assertTrue("'Odessa' is not found", cityDropdown.lnk_city_odessa.isDisplayed());
    }

    @Test
    public void testEmptyCart() throws Exception {
        cart = mainPage.clickOnCartBtn();
        assertTrue("Cart is NOT empty", cart.emptyCart.isDisplayed());
		mainPage = cart.closeCart();
    }
}
