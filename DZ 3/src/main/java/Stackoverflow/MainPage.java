package Stackoverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertTrue;

public class MainPage {
    private WebDriver driver;

    public MainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[@id='tabs']/a[@data-value='featured']/span")
    public WebElement lnk_number;

    @FindBy(xpath = ".//div[@class='links-container']//a[contains(@href, 'signup')]")
    public WebElement lnk_signup;

    @FindBy(xpath = ".//a[@class='question-hyperlink']")
    public WebElement lnk_question;


    public void CheckSum() throws Exception {
        String TxtBoxContent = lnk_number.getText();
        int sum = Integer.parseInt(TxtBoxContent);
        assertTrue("Featured number > 300", sum > 300);
    }

    public SignUp clickOnSingUpLnk() {
        lnk_signup.click();
        return new SignUp(driver);
    }

    public Questions clickOnQuestionsLnk() {
        lnk_question.click();
        return new Questions(driver);
    }

}
