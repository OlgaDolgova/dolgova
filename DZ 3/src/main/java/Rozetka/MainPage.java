package Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage {
    private WebDriver driver;

    public MainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[@id='body-header']//div[@class='logo']/img")
    public WebElement img_mainpage_logo;

    @FindBy(xpath = ".//a[contains(., 'Apple')]")
    public WebElement btn_mainpage_apple;

    @FindBy(xpath = "//*[@id='m-main']/li/a[contains(text(), 'MP3')]")
    public WebElement btn_mainpage_MP3section;

    @FindBy(xpath = ".//*[@id='city-chooser']/a[@name='city']")
    public WebElement btn_mainpage_city;

    @FindBy(xpath = ".//*[@id='body-header']//*[contains(text(), 'Корзина')]")
    public WebElement btn_mainpage_cart;

    public CityDropdown clickOnCityDropdown() {
        btn_mainpage_city.click();
        return new CityDropdown(driver);
    }

    public Cart clickOnCartBtn() {
        btn_mainpage_cart.click();
        return new Cart(driver);
    }
}
