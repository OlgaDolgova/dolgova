package Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class CityDropdown {
    private WebDriver driver;

    public CityDropdown(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[@class='header-city-i']/*[contains(text(),'Киев')]")
    public WebElement lnk_city_kiev;

    @FindBy(xpath = ".//*[@class='header-city-i']/*[contains(text(),'Одесса')]")
    public WebElement lnk_city_odessa;

    @FindBy(xpath = ".//*[@class='header-city-i']/*[contains(text(),'Харьков')]")
    public WebElement lnk_city_kharkov;

}