package Rozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class Cart {

    public WebDriver driver;

    public Cart (WebDriver driver){
        PageFactory.initElements(driver,this);
        this.driver=driver;
    }

    @FindBy(xpath=".//*[@class='wrap-cart-empty']")
    public WebElement emptyCart;
	
	@FindBy(xpath=".//*[@id='cart-popup']/a/img")
    public WebElement btn_x_for_cart;

    public MainPage closeCart() {
        btn_x_for_cart.click();
        return new MainPage(driver);
    }
}