import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.*;
import org.openqa.selenium.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class Stackoverflow {

    static WebDriver driver;
    private static String baseURL = "http://stackoverflow.com/";

    @BeforeClass
    public static void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.get(baseURL);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
    }

    @Before
    public void setUpBefore() throws Exception {
        if (driver == null) {
            setUp();
        } else {
            if (!driver.getCurrentUrl().equals(baseURL)) {
                driver.get(baseURL);
            }
        }
    }

    @AfterClass
    public static void afterMethod() throws Exception {
        driver.close();
    }

    @Test
    public void testNumberOfFeatured() throws Exception {
        driver.findElement(By.xpath(".//*[@id='tabs']/a[@data-value='featured']/span")).click();
        String number = driver.findElement(By.xpath(".//*[@id='tabs']/a[@data-value='featured']/span")).getText();
        int sum = Integer.parseInt(number);
        Assert.assertTrue("Featured number > 300", sum > 300);
    }

    @Test
    public void testSingUp() throws Exception {
        driver.findElement(By.xpath(".//div[@class='links-container']//a[contains(@href, 'signup')]")).click();
        WebElement Fbutton = driver.findElement(By.xpath(".//*[@id='openid-buttons']//div[@class='text']/span[contains(text(), 'Facebook')]/.."));
        WebElement Gbutton = driver.findElement(By.xpath(".//*[@id='openid-buttons']//div[@class='text']/span[contains(text(), 'Google')]/.."));
        Assert.assertNotNull("'Google' button is absent on login page", Gbutton);
        Assert.assertNotNull("'Facebook' button is absent on login page", Fbutton);
    }

    @Test
    public void testQuestionDate() throws Exception {
        driver.findElement(By.xpath(".//a[@class='question-hyperlink']")).click();
        String day = driver.findElement(By.xpath("//*[@id='qinfo']//p/b[contains(., 'today')]")).getText();
        Assert.assertTrue("Questions was asked not today", day.equals("today"));
    }
}
