import org.junit.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;


public class Rozetka {

    static WebDriver driver;
    private static String baseURL = "http://rozetka.com.ua/";

    @BeforeClass
    public static void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.get(baseURL);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

    @Before
    public void setUpBefore() throws Exception {
        if (driver == null) {
            setUp();
        } else {
            if (!driver.getCurrentUrl().equals(baseURL)) {
                driver.get(baseURL);
            }
        }
    }

    @AfterClass
    public static void afterMethod() throws Exception {
        driver.close();
    }

    @Test
    public void testMainIcon() throws Exception {
        Assert.assertTrue("The logo is absent", driver.findElement(By.xpath(".//*[@id='body-header']//div[@class='logo']/img")).isDisplayed());
    }

    @Test
    public void testRightMenu() throws Exception {
        WebElement submenu = driver.findElement(By.xpath(".//a[contains(., 'Apple')]"));
        Assert.assertTrue("'Apple' submenu is absent", submenu.isDisplayed());
        TimeUnit.SECONDS.sleep(5);
    }

    @Test
    public void testRightSubMenu() throws Exception {
        WebElement submenu1 = driver.findElement(By.xpath(".//*[@id='m-main']/li[@menu_id='47']/a[contains(@href,'mp3')]"));
        Assert.assertTrue("'MP3' section is absent", submenu1.isDisplayed());
    }

    @Test
    public void testCitiesDropDown() throws Exception {
        driver.findElement(By.xpath(".//*[@id='city-chooser']/a[@name='city']")).click();
        Assert.assertTrue("Can't find  Киев among the cities", driver.findElement(By.xpath(".//*[@class='header-city-i']/*[contains(text(),'Киев')]")).isDisplayed());
        Assert.assertTrue("Can't find  Одесса among the cities", driver.findElement(By.xpath(".//*[@class='header-city-i']/*[contains(text(),'Одесса')]")).isDisplayed());
        Assert.assertTrue("Can't find  Харьков among the cities", driver.findElement(By.xpath(".//*[@class='header-city-i']/*[contains(text(),'Харьков')]")).isDisplayed());
    }

    @Test
    public void testEmptyCart() throws Exception {
        driver.findElement(By.xpath(".//a[contains(@href, 'cart')]")).click();
        String TxtBoxContent = driver.findElement(By.xpath(".//*[@id='drop-block']/h2")).getText();
        Assert.assertTrue("Cart is not empty", TxtBoxContent.equals("Корзина пуста"));
        //driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.findElement(By.xpath(".//*[@id='cart-popup']/a/img")).click();

    }
}
