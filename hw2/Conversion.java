package hw2;

import java.io.UnsupportedEncodingException;

public class Conversion {
    public static void main(String[] args) {

        //String to numbers

        //String to byte
        //конструктор
        try {
            Byte b1 = new Byte("103");
            System.out.println(b1);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }
        //метод valueOf класса Byte
        String str1 = "11";
        try {
            Byte b2 = Byte.valueOf(str1);
            System.out.println(b2);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }
        //метод parseByte класса Byte
        byte b = 0;
        String str2 = "100";
        try {
            b = Byte.parseByte(str2);
            System.out.println(b);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }
        //Перевод строки в массив байтов и обратно из массива байтов в строку
        String str3 = "0022";
        byte[] b3 = str3.getBytes();
        System.out.println(b3);
        //массив байтов переводим обратно в строку
        try {
            String s = new String(b3, "cp1251");
            System.out.println(s);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }


        //String to short
        //конструктор
        try {
            Short s1 = new Short("333");
            System.out.println(s1);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }

        //метод valueOf класса Short
        String str4 = "59";
        try {
            Short s2 = Short.valueOf(str4);
            System.out.println(s2);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }
        //метод parseShort класса Short
        short s3 = 0;
        String str5 = "54";
        try {
            s3 = Short.parseShort(str5);
            System.out.println(s3);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }

        //String to int
        //конструктор
        try {
            Integer i1 = new Integer("20349");
            System.out.println(i1);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }
        //метод valueOf класса Integer
        String str6 = "1451";
        try {
            Integer i2 = Integer.valueOf(str6);
            System.out.println(i2);
        }catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }

        //метод parseInt класса Integer
        int i3 = 0;
        String str7 = "102944";
        try {
            i3 = Integer.parseInt(str7);
            System.out.println(i3);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }

        //String to long
        //конструктор
        try {
            Long l1 = new Long("11111111");
            System.out.println(l1);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }
        //метод valueOf класса Long
        String str8 = "222222222";
        try {
            Long l2 = Long.valueOf(str8);
            System.out.println(l2);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }
        //метод parseLong класса Long
        long l3 = 0;
        String str9 = "333333333";
        try {
            l3 = Long.parseLong(str9);
            System.out.println(l3);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }


        //String to float
        //конструктор
        try {
            Float f1 = new Float("123.5");
            System.out.println(f1);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }
        //метод valueOf класса Float
        String str10 = "3.1415";
        try {
            Float f2 = Float.valueOf(str10);
            System.out.println(f2);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }
        //метод parseFloat класса Float
        float f3 = 0;
        String str11 = "36.6";
        try {
            f3 = Float.parseFloat(str11);
            System.out.println(f3);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }

        //String to double
        //конструктор
        try {
            Double d1 = new Double("4.4e10");
            System.out.println(d1);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }
        //метод valueOf класса Double
        String str12 = "564.6e10";
        try {
            Double d2 = Double.valueOf(str12);
            System.out.println(d2);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }
        //метод parseDouble класса Double
        double d3 = 0;
        String str13 = "13.7e10";
        try {
            d3 = Double.parseDouble(str13);
            System.out.println(d3);
        } catch (NumberFormatException e) {
            System.err.println("Неверный формат строки!");
        }

        //String to boolean
        String s4 = "True";
        String s5 = "yes";
        boolean bool1, bool2, boolean1, boolean2;
        //метод parseBoolean
        bool1 = Boolean.parseBoolean(s4);
        bool2 = Boolean.parseBoolean(s5);
        System.out.println( bool1 );
        System.out.println( bool2 );
        //метод valueOf
        boolean1 = Boolean.valueOf(s4);
        boolean2 = Boolean.valueOf(s5);
        System.out.println( boolean1 );
        System.out.println( boolean2 );


        //Number to String

        //int to String
        int i4 = 57;
        String str14 = Integer.toString(i4);
        System.out.println(str14);

        //double to String
        double  i5 = 32.4e10;
        String str15 = Double.toString(i5);
        System.out.println(str15);

        //long to String
        long  i6 = 223333334;
        String str16 = Long.toString(i6);
        System.out.println(str16);

        //float to String
        float  i7 = 3.463f;
        String str17= Float.toString(i7);
        System.out.println(str17);

        //Char
        //char to String
        char ch = 'S';
        // c использованием класса Character
        String charToString = Character.toString(ch);
        System.out.println(charToString);

        // с использованием операции добавления класса String
        String str18 = "" + ch;
        System.out.println(str18);

        //с использованием массива
        String fromChar = new String(new char[] { ch });
        System.out.println(fromChar);

        // с использованием метода valueOf класса String
        String valueOfchar = String.valueOf(ch);
        System.out.println(valueOfchar);


        //char to int

        char ch1 = '9';

        // c использованием метода getNumericValue класса Character
        int i8 = Character.getNumericValue(ch1);
        System.out.println(i8);

        // c использованием метода digit класса Character
        int i9 = Character.digit(ch1,10);
        System.out.println(i9);

        //Numbers

        //int to long
        int i10 = 2016;
        long l = (long) (i10);
        System.out.println(l);

        //int to float
        int i11 = 2016;
        float f = (float) (i11);
        System.out.println(f);

        //long to int
        long l1 = 2147483644;
        int i12 = (int) l1;
        System.out.println(i12);

        //double to int
        double d = 3.14;
        int i13 = (int) d;
        System.out.println(i13);
    }
}
