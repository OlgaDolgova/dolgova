package hw2;
import java.text.DecimalFormat;
import java.util.Scanner;

public class MathOperations {
    public static void main(String[] args) {
        String str = "", str2 = "";
        double result = 0;
        double firstNumber = 0, secondNumber = 0;
        String mathSign = "";
        boolean t = true;
        Scanner sc;


        System.out.println("Введите число:");
        while (t) {
            sc = new Scanner(System.in);
            str = sc.next();

            try {
                firstNumber = Double.parseDouble(str);
                t = false;
            } catch (NumberFormatException e) {
                System.err.println("Неверный формат строки, введите число");
                t = true;
            }
        }

        System.out.println("Введите математический знак: + или - или * или /");

        while (true) {
            sc = new Scanner(System.in);
            str = sc.next();
            mathSign = str;
            if (str.equals("/")) break;
            if (str.equals("*")) break;
            if (str.equals("+")) break;
            if (str.equals("-")) break;

            System.err.println("Неверный формат строки, введите математический знак: + или - или * или /");
        }

        t = true;
        System.out.println("Введите число:");
        while (t) {
            sc = new Scanner(System.in);
            str2 = sc.next();
            try {
                secondNumber = Double.parseDouble(str2);
                t = false;
            } catch (NumberFormatException e) {
                System.err.println("Неверный формат строки, введите число");
                t = true;
            }
        }
        switch (mathSign) {
            case "+":
                result = firstNumber + secondNumber;
                break;
            case "-":
                result = firstNumber - secondNumber;
                break;
            case "*":
                result = firstNumber * secondNumber;
                break;
            case "/":
                result = firstNumber / secondNumber;
                break;
        }

            System.out.println(convert(firstNumber) + " " + mathSign + " " + convert(secondNumber) + " = " + convert(result));
    }

    private static String convert(double digit){
        DecimalFormat df = new DecimalFormat("0.###");
        return df.format(digit);
    }
}


