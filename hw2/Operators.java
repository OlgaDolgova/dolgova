package hw2;
public class Operators {
    public static void main (String[] args){
        int a, b, x, y;
        a = 5;
        b = 7;
        x = 1;
        y = 0;

        //сложение
        System.out.println(a+b);

        //вычитание
        System.out.println(a-b);

        //умноженеи
        System.out.println(a*b);

        //деление без остатка
        System.out.println(a/b);

        //деление с остатком
        double c = a;
        System.out.println(c/b);

        //остаток от деления
        System.out.println(b%a);

        // Relational operators

        if(a == b)
            System.out.println("a равно b");

        if(a != b)
            System.out.println("a не равно b");

        if(a > b)
            System.out.println("a больше b");

        if(a < b)
            System.out.println("a меньше b");

        if(a <= b)
            System.out.println("a меньше либо равно b");

        // Bitwise operators
        int bitmask = 0x000F;
        int val = 0x000d;
        System.out.println(val & bitmask);
        System.out.println(val | bitmask);
        System.out.println(val ^ bitmask);

        //Logical operators

        // таблица истинности для логичкского И (0-0 1-0 0-1 1-1)
        System.out.println(y & y);
        System.out.println(x & y);
        System.out.println(y & x);
        System.out.println(x & x);

        // таблица истинности для логического ИЛИ (0-0 1-0 0-1 1-1)
        System.out.println(y | y);
        System.out.println(x | y);
        System.out.println(y | x);
        System.out.println(x | x);

        // таблица истинности для исключающего ИЛИ (0-0 1-0 0-1 1-1)
        System.out.println(y | y);
        System.out.println(x | y);
        System.out.println(y | x);
        System.out.println(x | x);

        //Assignment operators

        //Conditional operators
        int value1 = 1;
        int value2 = 2;
        if((value1 == 1) && (value2 == 2))
            System.out.println("value1 is 1 AND value2 is 2");
        if((value1 == 1) || (value2 == 2))
            System.out.println("value1 is 1 OR value2 is 1");

        int value3 = 4;
        int value4 = 5;
        int result;
        boolean someCondition = true;
        result = someCondition ? value3 : value4;
        System.out.println(result);

        //Instanceof operators http://www.javatpoint.com/downcasting-with-instanceof-operator
        Operators o = new Operators();
        System.out.println(o instanceof Operators);//true

        //Precedence http://introcs.cs.princeton.edu/java/11precedence/
        System.out.println("1 + 2 = " + 1 + 2);
        System.out.println("1 + 2 = " + (1 + 2));

    }
}


