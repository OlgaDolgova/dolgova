package hw7;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        String filename = "avto.txt";
        Main main = new Main();
        main.serializeObjects(filename);
        main.deserializeObjects(filename);
    }

    public void serializeObjects(String writePlace) {
        System.out.println("Сериализация:");
        try {
            Avto a1 = new AvtoSuccessor("audi", "TT", "red", 2010, 1000, 350, 20, 10);
            Avto a2 = new AvtoSuccessor("bmw", "X5", "red", 2012, 1000, 350, 20, 10);
            Avto a3 = new AvtoSuccessor("qq", "ss", "ddd", 1000, 25, 400, 0.4, 20);
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(writePlace));
            oos.writeObject(a1);
            oos.writeObject(a2);
            oos.writeObject(a3);
            oos.close();
            a1.getAge();
            a2.getAge();
            a3.getAge();
            a1.getTime();
            a2.getTime();
            a3.getTime();
            a1.getInfoAboutAvto();
            a2.getInfoAboutAvto();
            a3.getInfoAboutAvto();
            System.out.println("Запись произведена");
            System.out.println();
        } catch (IOException ex) {
            System.out.println("Невозможно записать сериализованые объекты в " + writePlace + ". Листинг :" + ex.getMessage());
        }
    }

    public List<Avto> deserializeObjects(String readPlace) {
        List<Avto> autoList = new ArrayList<>();
        System.out.println("Десериализация:");


        try {
            //чтение из файла
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(readPlace));
            Avto a1 = (AvtoSuccessor) ois.readObject();
            Avto a2 = (AvtoSuccessor) ois.readObject();
            Avto a3 = (AvtoSuccessor) ois.readObject();

            Field brand = Avto.class.getDeclaredField("brand");
            brand.setAccessible(true);
            String brandFieldValue = (String) brand.get(a1);
            Field model = Avto.class.getDeclaredField("model");
            model.setAccessible(true);
            String modelFieldValue = (String) model.get(a1);
            Field color = Avto.class.getDeclaredField("color");
            color.setAccessible(true);
            String colorFieldValue = (String) color.get(a1);
            Field weight = Avto.class.getDeclaredField("weight");
            weight.setAccessible(true);
            Double weightFieldValue = (Double) weight.get(a1);
            Field Vmax = Avto.class.getDeclaredField("Vmax");
            Vmax.setAccessible(true);
            Double VmaxFieldValue = (Double) Vmax.get(a1);
            brand.setAccessible(true);
            String brandFieldValue1 = (String) brand.get(a2);
            model.setAccessible(true);
            String modelFieldValue1 = (String) model.get(a2);
            color.setAccessible(true);
            String colorFieldValue1 = (String) color.get(a2);
            weight.setAccessible(true);
            Double weightFieldValue1 = (Double) weight.get(a2);
            Vmax.setAccessible(true);
            Double VmaxFieldValue1 = (Double) Vmax.get(a2);
            brand.setAccessible(true);
            String brandFieldValue2 = (String) brand.get(a3);
            model.setAccessible(true);
            String modelFieldValue2 = (String) model.get(a3);
            color.setAccessible(true);
            String colorFieldValue2 = (String) color.get(a3);
            weight.setAccessible(true);
            Double weightFieldValue2 = (Double) weight.get(a3);
            Vmax.setAccessible(true);
            Double VmaxFieldValue2 = (Double) Vmax.get(a3);

            System.out.println("марка: " + brandFieldValue + "; " + "модель: " + modelFieldValue + "; " + "цвет: " + colorFieldValue + "; " + "возраст: " + a1.getAge() + "; " + "вес: " + weightFieldValue + "; " + "максимальная скорость: " + VmaxFieldValue + "; ");
            System.out.println("марка: " + brandFieldValue1 + "; " + "модель: " + modelFieldValue1 + "; " + "цвет: " + colorFieldValue1 + "; " + "возраст: " + a2.getAge() + "; " + "вес: " + weightFieldValue1 + "; " + "максимальная скорость: " + VmaxFieldValue1 + "; ");
            System.out.println("марка: " + brandFieldValue2 + "; " + "модель: " + modelFieldValue2 + "; " + "цвет: " + colorFieldValue2 + "; " + "возраст: " + a3.getAge() + "; " + "вес: " + weightFieldValue2 + "; " + "максимальная скорость: " + VmaxFieldValue2 + "; ");
            ois.close();

            autoList.add(a1);
            autoList.add(a2);


        } catch (IOException ex) {
            System.out.println("Невозможно считать десериализованые объекты из " + readPlace + ". Листинг :" + ex.getMessage());
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        return autoList;
    }

    public List<Avto> doReflection() {
        Avto a1 = new AvtoSuccessor("audi", "TT", "red", 2010, 1000, 350, 20, 10);
        Avto a2 = new AvtoSuccessor("bmw", "X5", "red", 2012, 1000, 350, 20, 10);

        System.out.println("\nРефлексия:");

        Class clazz = a1.getClass();
        Field field = null;
        try {
            field = clazz.getSuperclass().getDeclaredField("color");
            field.setAccessible(true);
            field.set(a1, "pink");
            System.out.println("2nd машина перекрашена в " + a1.getColor());

            Method method = a1.getClass().getMethod("getInfoAboutAvto");
            System.out.println("первая машина: ");
            method.invoke(a1);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        List<Avto> finalAutoList = new ArrayList<>();
        finalAutoList.add(a1);
        finalAutoList.add(a2);

        return finalAutoList;
    }
}
