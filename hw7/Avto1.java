package hw7;

import java.util.Calendar;
import java.util.StringJoiner;

public class Avto1 extends Avto {
    protected double weight;
    protected String brand;
    protected String model;
    protected String color;
    protected int yearOfRelease;

    Avto1(double Vmax, double man, double acc) {
        super(Vmax, man, acc);
    }

    Avto1(String brand, String model, String color, int yearOfRelease, double weight, double Vmax, double man, double acc) {
        super(Vmax, man, acc);
        this.brand = brand;
        this.model = model;
        this.color = color;
        this.yearOfRelease = yearOfRelease;
        this.weight = weight;
    }

    @Override
    public void turn() {
        if (Vcurrent > Vmax / 2) {
            Vo = (man + ((Vmax / 2 - Vcurrent) * 0.005)) * Vcurrent;
        } else {
            Vo = Vcurrent * man;
        }
    }

    public int getAge(){
        int age = Calendar.getInstance().get(Calendar.YEAR) - yearOfRelease;
        return age;
    }

    public void getInfoAboutAvto()
    {
        System.out.println("марка: "+ brand + "; " + "модель: " + model + "; " + "цвет: " + color + "; " + "возраст: " + getAge() + "; " + "вес: " + weight + "; " + "максимальная скорость: " + Vmax + "; ");
    }

    public void setColor(String color){
        this.color = color;
    }
}