package hw7;

import org.junit.Assert;
import org.junit.Test;
import sun.plugin2.message.Serializer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class MyTest extends Assert {
    @Test
    public void testSerial() {
        Main main = new Main();
        String filename = "avto_test_des.txt";
        main.serializeObjects(filename);
        File file = new File("avto_test_des.txt");
        assertTrue(file.exists());
        assertTrue(file.length() > 0);
    }

    @Test
    public void testDeserial() {
        Main main = new Main();

        List<Avto> initialList = new ArrayList<>();
        Avto a1 = new AvtoSuccessor("audi", "TT", "red", 2010, 1000, 350, 20, 10);
        Avto a2 = new AvtoSuccessor("bmw", "X5", "red", 2012, 1000, 350, 20, 10);
        Avto a3 = new AvtoSuccessor("qq","ss","ddd",1000,25,400,0.4,20);
        initialList.add(a1);
        initialList.add(a2);

        String filename = "avto_test_des2.txt";
        main.serializeObjects(filename);
        List<Avto> avtoListDeserialized = main.deserializeObjects(filename);
        assertTrue(initialList.equals(avtoListDeserialized));
    }

    @Test
    public void testDeserialFail() {
        Main main = new Main();

        List<Avto> initialList = new ArrayList<>();
        Avto a1 = new AvtoSuccessor("audi", "TT", "red", 2010, 1000, 350, 20, 10);
        Avto a2 = new AvtoSuccessor("asdkjfladkjsf", "X5", "red", 2012, 1000, 350, 20, 10);
        initialList.add(a1);
        initialList.add(a2);

        String filename = "avto_test_des1.txt";
        main.serializeObjects(filename);
        List<Avto> avtoListDeserialized = main.deserializeObjects(filename);
        assertNotEquals(initialList, avtoListDeserialized);
    }

    @Test
    public void testDoReflection() {
        Main main = new Main();
        List<Avto> initialList = new ArrayList<>();
        Avto a1 = new AvtoSuccessor("audi", "TT", "pink", 2010, 1000, 350, 20, 10);
        Avto a2 = new AvtoSuccessor("bmw", "X5", "red", 2012, 1000, 350, 20, 10);
        initialList.add(a1);
        initialList.add(a2);

        List<Avto> listAfterReflection = main.doReflection();
        assertTrue(initialList.equals(listAfterReflection));

    }
}
