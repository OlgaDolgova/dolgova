package hw7;

import java.io.Serializable;
import java.util.Calendar;

public class Avto implements Serializable {
    private double Vmax;
    private double man;
    private double acc;
    private double Vcurrent;
    private double Vo;
    private double time;
    private double weight;
    private String brand;
    private String model;
    private String color;
    private int yearOfRelease;
    public static final double distance = 2000;

    public String getColor() {
        return color;
    }

    public static void main(String[] args) {
    }

    public Avto(double Vmax, double man, double acc) {
        this.Vmax = Vmax;
        this.man = man;
        this.acc = acc;
    }

    public Avto(double Vmax, double man, double acc, double Vcur, double time) {
        this(Vmax, man, acc);
        this.Vcurrent = Vcur;
        this.time = time;
    }

    public Avto(String brand, String model, String color, int yearOfRelease, double weight, double Vmax, double man, double acc) {
        this(Vmax, man, acc);
        this.brand = brand;
        this.model = model;
        this.color = color;
        this.yearOfRelease = yearOfRelease;
        this.weight = weight;
    }

    public double getVmax() {
        return Vmax;
    }

    public void setVmax(double vmax) {
        Vmax = vmax;
    }

    public double getMan() {
        return man;
    }

    public void setMan(double man) {
        this.man = man;
    }

    public double getAcc() {
        return acc;
    }

    public void setAcc(double acc) {
        this.acc = acc;
    }

    public double getVcur() {
        return Vcurrent;
    }

    public void setVcur(double vcur) {
        Vcurrent = vcur;
    }

    public double getDist() {
        return distance;
    }

    public double getVo() {
        return Vo;
    }

    public void setVo(double vo) {
        Vo = vo;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }


    public void move() {
        int distance1;
        Vcurrent = (int) Math.sqrt(2 * distance * acc + Math.pow(Vcurrent, 2));
        if (Vcurrent > Vmax) {
            distance1 = (int) ((Math.pow(Vmax, 2) - Math.pow(Vo, 2)) / (2 * acc));
            time += (Vmax - Vo) / acc;
            time += ((distance - distance1) / Vmax);
        } else {
            time += (Vcurrent - Vo) / acc;
        }

    }

    public void conversion() {
        Vcurrent = Vcurrent * 1000 / 3600;
    }


    public void turn() {
        if (Vcurrent > Vmax / 2) {
            Vo = (man + ((Vmax / 2 - Vcurrent) * 0.005)) * Vcurrent;
        } else {
            Vo = Vcurrent * man;
        }
    }

    public int getAge() {
        int age = Calendar.getInstance().get(Calendar.YEAR) - yearOfRelease;
        return age;
    }

    public void getInfoAboutAvto() {
        System.out.println("марка: " + brand + "; " + "модель: " + model + "; " + "цвет: " + color + "; " + "возраст: " + getAge() + "; " + "вес: " + weight + "; " + "максимальная скорость: " + Vmax + "; ");
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Avto avto = (Avto) o;

        if (!brand.equals(avto.brand)) return false;
        if (!model.equals(avto.model)) return false;
        return color.equals(avto.color);

    }

    @Override
    public int hashCode() {
        int result = brand.hashCode();
        result = 31 * result + model.hashCode();
        result = 31 * result + color.hashCode();
        return result;
    }
}
