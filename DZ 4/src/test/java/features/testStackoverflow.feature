@Stack @RunAllTests
Feature: Stackoverflow Feature

  Background:
    Given User navigates to main page of Stackoverflow website

  @001 @MySelection
  Scenario: Check quantity on featured tab is more than 300 within main page
    Then User sees the quantity on featured tab is more than 300

  @002
  Scenario: Check that Google and Facebook buttons are present in signUp page
    When User clicks on 'SignUP' link
    Then The buttons Google and Facebook are present

  @003 @MySelection
  Scenario: Check that top question has been asked today
    When User clicks on question button to navigate to Question page
    Then User sees "today" element


