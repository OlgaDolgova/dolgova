@Rozetka @RunAllTests
Feature: Rozetka Feature

  @001 @MySelection
  Scenario: Check the displaying of main logo
    Given User navigates to the main page of "Rozetka" website
    Then Rozetka logo is present at the left top corner

  @002
  Scenario: Check that 'MP3' section is present in catalogue
    Given User navigates to the main page of "Rozetka" website
    Then Catalog menu contains a section with "MP3" text

  @003 @MySelection
  Scenario: Check that Apple submenu is present in catalogue
    Given User navigates to the main page of "Rozetka" website
    Then Catalog menu contains "Apple" submenu

  @004
  Scenario: Check that cities are present in dropdown menu
    Given User navigates to the main page of "Rozetka" website
    When User clicks on "Select city"
    Then Kharkiv and Kiev and Odessa cities are present within dropdown

  @005
  Scenario: Check that cart is empty
    Given User navigates to the main page of "Rozetka" website
    When User clicks on "Cart" link
    Then The cart is empty