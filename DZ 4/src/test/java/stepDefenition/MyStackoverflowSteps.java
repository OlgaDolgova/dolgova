package stepDefenition;

import Stackoverflow.MainPage;
import Stackoverflow.Questions;
import Stackoverflow.SignUp;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import runners.FeatureRunner;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;


public class MyStackoverflowSteps {
    private static Questions questions;
    private static SignUp signUp;
    private MainPage mainPage;
    private static String baseUrl = "http://stackoverflow.com";

    public MyStackoverflowSteps() throws Exception {}

    @Given("^User navigates to main page of Stackoverflow website$")
    public void userNavigatesToMainPageOfStackoverflowWebsite() throws Throwable {
        FeatureRunner.driver.get(baseUrl);
        FeatureRunner.driver.manage().window().maximize();
        FeatureRunner.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        mainPage = new MainPage(FeatureRunner.driver);
    }

    @Then("^User sees the quantity on featured tab is more than 300")
    public void User_sees_the_quantity_on_featured_tab_is_more_than_300() throws Throwable {
        mainPage.CheckSum();
    }

    @When("^User clicks on 'SignUP' link$")
    public void User_clicks_on_SignUP_link() throws Throwable {
        signUp = mainPage.clickOnSingUpLnk();
    }

    @Then("^The buttons Google and Facebook are present$")
    public void The_buttons_Google_and_Facebook_are_present() throws Throwable {
        assertNotNull("'Google' button is absent on login page", signUp.btn_google);
        assertNotNull("'Facebook' button is absent on login page", signUp.btn_facebook);
    }

    @When("^User clicks on question button to navigate to Question page$")
    public void User_clicks_on_question_button_to_navigate_to_Question_page() throws Throwable {
        questions = mainPage.clickOnQuestionsLnk();
    }

    @Then("^User sees \"([^\"]*)\" element$")
    public void userSeesElement(String arg0) throws Throwable {
        assertTrue("Questions was asked NOT today", questions.asked_day.getText().equals("today"));
    }

}