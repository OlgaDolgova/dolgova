package stepDefenition;

import Rozetka.*;
import cucumber.api.java.en.*;
import runners.FeatureRunner;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertTrue;

public class MyRozetkaSteps {
    private MainPage mainPage;
    private CityDropdown cityDropdown;
    private Cart cart;
    private String baseUrl = "http://rozetka.com.ua/";

    public MyRozetkaSteps() throws Exception {    }

    @Given("^User navigates to the main page of \"(.*?)\" website$")
    public void user_navigates_to_the_main_page_of_website(String arg1) throws Throwable {
        FeatureRunner.driver.get(baseUrl);
        FeatureRunner.driver.manage().window().maximize();
        FeatureRunner.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        mainPage = new MainPage(FeatureRunner.driver);
    }

    @Then("^Rozetka logo is present at the left top corner$")
    public void rozetka_logo_is_present_at_the_left_top_corner() throws Throwable {
        assertTrue("The logo is absent", mainPage.img_mainpage_logo.isDisplayed());
    }

    @Then("^Catalog menu contains a section with \"([^\"]*)\" text$")
    public void Catalog_menu_contains_a_section_with_text(String arg0) throws Throwable {
        assertTrue("'MP3' section is absent", mainPage.btn_mainpage_MP3section.isDisplayed());
    }

    @Then("^Catalog menu contains \"([^\"]*)\" submenu$")
    public void Catalog_menu_contains_submenu (String arg0) throws Throwable{
        assertTrue("'Apple' submenu is absent", mainPage.btn_mainpage_apple.isDisplayed());
    }

    @When("^User clicks on \"(.*?)\"$")
    public void user_clicks_on(String arg1) throws Throwable {
        this.cityDropdown = this.mainPage.clickOnCityDropdown();
    }

    @Then("^Kharkiv and Kiev and Odessa cities are present within dropdown$")
    public void kharkivAndKievAndOdessaCitiesArePresentWithinDropdown() throws Throwable {
        assertTrue("'Kharkiv' is not found", cityDropdown.lnk_city_kharkov.isDisplayed());
        assertTrue("'Kiev' is not found", cityDropdown.lnk_city_kiev.isDisplayed());
        assertTrue("'Odessa' is not found", cityDropdown.lnk_city_odessa.isDisplayed());
    }

    @When("^User clicks on \"(.*?)\" link$")
    public void user_clicks_on_link(String arg1) throws Throwable {
        this.cart = this.mainPage.clickOnCartBtn();
    }

    @Then("^The cart is empty$")
    public void The_cart_is_empty() throws Throwable {
        assertTrue("Cart is NOT empty", cart.emptyCart.isDisplayed());
    }
}
