package runners;

import cucumber.api.CucumberOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import cucumber.api.junit.Cucumber;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


@RunWith(Cucumber.class)

@CucumberOptions(monochrome = true,
		// @MySelection - 2tests from each set
        // @Rozetka - all tests for Rozetka
        // @Stack - all tests for Stackoverflow
        // @RunAllTests - all tests for both Stackoverflow and Rozetka
        tags = "@MySelection",
        features = "src/test/java/features/",
        glue = "stepDefenition",
        plugin = {"html:target/cucumber-report/smoketest", "json:target/cucumber.json"}
        //        format = {"pretty", "html: cucumber-html-reports",
//                "json: cucumber-html-reports/cucumber.json"}
)

public class FeatureRunner {
    public static WebDriver driver;


    @BeforeClass
    public static void setUp() throws Exception {
        driver = new FirefoxDriver();
    }


    @AfterClass
    public static void tearDown() throws Exception {
        driver.quit();
    }
}
