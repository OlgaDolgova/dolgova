package Stackoverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SignUp {
    private WebDriver driver;

    public SignUp(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[@id='openid-buttons']//div[@class='text']/span[contains(text(), 'Google')]/..")
    public WebElement btn_google;

    @FindBy(xpath = ".//*[@id='openid-buttons']//div[@class='text']/span[contains(text(), 'Facebook')]/..")
    public WebElement btn_facebook;
}
