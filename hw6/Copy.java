package hw6;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Copy implements Runnable {
    private final String sourceDir;
    private final String destDir;
    private final String sPoint;

    Copy(String sourceDir, String destDir, String sPoint) {
        this.sourceDir = sourceDir;
        this.destDir = destDir;
        this.sPoint = sPoint;
    }


    @Override
    public void run() {
        long startTime = System.currentTimeMillis(); // таймер когда стартанули
        long timeSpent = 0;
        Date moment = new Date(startTime);
        SimpleDateFormat ft = new SimpleDateFormat("hh:mm:ss");
        System.out.println("The new thread started at: " + ft.format(moment));

        Copy.getStructureOfDirectory(0, sourceDir, sPoint);
        Copy.copyStructureOfDirectory(sourceDir, destDir, sPoint, 0);
        Copy.recursiveDelete(new File(destDir));

        timeSpent = System.currentTimeMillis() - startTime; // запомнили когда закончилось успешное копирование
        System.out.println("Copy below up to " + timeSpent + " milliseconds");
    }


    public static void recursiveDelete(File file) {
        // до конца рекурсивного цикла
        if (!file.exists())
            return;

        //если это папка, то идем внутрь этой папки и вызываем рекурсивное удаление всего, что там есть
        if (file.isDirectory()) {
            for (File f : file.listFiles()) {
                // рекурсивный вызов
                recursiveDelete(f);
            }
        }
        // вызываем метод delete() для удаления файлов и пустых(!) папок
        if (file.isDirectory()) {
            file.delete();
        }
    }

    // получение структуры папок в виде дерева
    public static void getStructureOfDirectory(int level, String path, String sPoint) {
        String indent = "";
        for (int i = 0; i < level; i++) {
            indent += ".";
        }

        File actual = new File(path);
        for (File f : actual.listFiles()) {

            System.out.printf("%s %s\n", indent, f.getName());

            if (f.isDirectory()) {
                level++;
                getStructureOfDirectory(level, f.getAbsolutePath(), sPoint);
            }
        }
    }


    // копирование каталога с файлами в овое место
    public static void copyStructureOfDirectory(String sourceDirPath, String destDirPath, String sPoint, int num) {

        File actual = new File(sourceDirPath);
        File copyDir = new File(destDirPath);
        String fileName = copyDir.getAbsolutePath();
        String s = "";
        String m = "";
        int i = 0;
        while (copyDir.exists()) {
            if (num != 0) {
                s = "(" + Integer.toString(num) + ")";
            }
            if (i != 0) {
                m = "(" + Integer.toString(i) + ")";
            }
            copyDir = new File(fileName.substring(0, fileName.length() - s.length()) + m);
            i++;
        }
        num = i;
        if (!copyDir.exists()) {
            copyDir.mkdir();
        }


        for (File f : actual.listFiles()) {
            String newFilePath = copyDir.getAbsolutePath() + "\\" + f.getName();

            if (f.isDirectory()) {
                copyStructureOfDirectory(f.getAbsolutePath(), newFilePath, sPoint, num);
            }
            {
                if (f.isFile()) {
                    if (f.getName().contains(sPoint)) {
                        copyFile(f.getAbsolutePath(), newFilePath);
                    }
                }
            }
        }
    }



    //копирование файла в новый файл
    public static void copyFile(String fullPathSource, String fullPathDestination) {
        BufferedReader reader = null;
        FileOutputStream output = null;

        try {
            FileReader fileReader = new FileReader(fullPathSource);
            reader = new BufferedReader(fileReader);
            output = new FileOutputStream(fullPathDestination);
            String line;
            while ((line = reader.readLine()) != null) {
                output.write(line.getBytes());
                output.write("\n".getBytes());
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
