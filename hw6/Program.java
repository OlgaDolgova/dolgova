package hw6;

import java.io.*;
import java.util.Scanner;


public class Program {

    // Method gets structure of folder
    public static void main(String[] args) {
        //String workingDir = System.getProperty("user.dir");
        String destDir = "D:\\testtest"; //куда копировать
        String sourceDir = "D:\\test"; //откуда копировать
        startCopy(sourceDir, destDir);
        System.out.println("Главный поток завершён...");
    }

    private static String getTypedText() throws IOException {
        System.out.println("Type part of file or full file name for copy");
        Scanner sc = new Scanner(System.in);
        return sc.nextLine();
    }

    private static void startCopy(String sourceDir, String destDir) {
		while (true) {
			try {
				String getText = getTypedText();
                if (getText.equals("exit")) break;//выход по ключевому слову "exit"
                Copy copier = new Copy(sourceDir, destDir, getText);
                Thread myTread = new Thread(copier);
                myTread.start();
			} catch (IOException ex) {
				System.out.println("The file by path '" + ex.getMessage() + "' didn't exist, please check path");
			}
		}
	}
}