package hw5;

import java.text.SimpleDateFormat;
import java.util.Date;


public class WorkingException extends RuntimeException {

    public WorkingException(Date d) {
        super(new SimpleDateFormat("dd-MMMM-yyyy").format(d) + " - Working day");
//        SimpleDateFormat format1 = new SimpleDateFormat("dd-MMMM-yyyy");
//        System.out.println(format1.format(d) + " - Working day");
    }
}