package hw5;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Main {
    public Main() throws ParseException {
    }

    public static void main(String[] args) throws WorkingException, ParseException, WeekendException {
        ArrayList<String> mydates = new ArrayList<>();
        mydates.add("01.11.1212");
        mydates.add("11.11.2030");
        mydates.add("23.11.3005");
        mydates.add("13.11.2012");
        mydates.add("12.12.2012");
        mydates.add("02.02.2002");
        mydates.add("03.02.2002");
        mydates.add("04.02.2002");
        mydates.add("05.02.2002");
        mydates.add("06.02.2002");
        mydates.add("07.02.2002");
        mydates.add("11.02.2012");
        mydates.add("10.03.2012");
        mydates.add("11.04.2013");
        mydates.add("12.05.2012");
        mydates.add("13.06.2012");
        mydates.add("15.07.2015");
        mydates.add("11.08.2012");
        mydates.add("10.09.2014");
        mydates.add("11.10.2012");
        mydates.add("12.11.2012");
        mydates.add("13.12.2012");
        mydates.add("15.11.2015");
        mydates.add("11.11.2008");
        mydates.add("10.11.1992");
        mydates.add("11.11.1993");
        mydates.add("12.11.1994");
        mydates.add("13.11.1995");
        mydates.add("15.11.1996");
        mydates.add("11.11.1997");
        mydates.add("10.11.1998");
        mydates.add("11.11.1999");
        mydates.add("12.11.2000");
        mydates.add("13.11.2001");
        mydates.add("15.11.2002");
        mydates.add("11.11.2003");

        for (int i = 0; i < mydates.size(); i++) {
            SimpleDateFormat sdf = new SimpleDateFormat();
            sdf.applyPattern("dd.MM.yyyy");
            Date docDate = sdf.parse(String.valueOf(mydates.get(i)));
            checkDay(docDate);
        }
    }

    private static void checkDay(Date d) {
        Calendar startDate = Calendar.getInstance();
        startDate.setTime(d);
        try {
            if (startDate.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || startDate.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
                throw new WeekendException(d);
            } else {
                throw new WorkingException(d);
            }
        } catch (WorkingException e1) {
			System.out.println(e1.getMessage());
        }
		catch (WeekendException e2) {
            System.out.println(e2.getMessage());
        }
    }
}

