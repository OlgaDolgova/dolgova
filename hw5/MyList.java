package hw5;

import java.util.*;
import java.util.function.UnaryOperator;


public class MyList implements List  {
    transient int size = 0;
    private transient Object[] elementData;


    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public boolean contains(Object o) {
                for (int i = 0; i < size; i++) {
                    if (o.equals(elementData[i]))
                                return true;
                    }
        return false;
    }


    @Override
    public Object[] toArray() {
        return Arrays.copyOf(elementData, size);
    }

    @Override
    public boolean add(Object e) {
      elementData = Arrays.copyOf(elementData, size + 1);
      elementData[size + 1] = e;
      return true;
    }

    @Override
    public void add(int index, Object e) {
        Arrays.copyOf(elementData, size + 1);
        for(int i = size-1 ; i > index ; i--) {
            elementData[i + 1] = elementData[i];
        }
        elementData[index] = e;
    }

    @Override
    public boolean remove(Object o) {
            for (int index = 0; index < size; index++)
              if (o.equals(elementData[index])) {
                  int numMoved = size - index - 1;
                  if (numMoved > 0)
                  System.arraycopy(elementData, index + 1, elementData, index, numMoved);
                   elementData[size-1] = null;
               return true;
               }
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
         Object[] a = c.toArray();
         int numNew = a.length;
         elementData = Arrays.copyOf(elementData, + numNew);
         System.arraycopy(a, 0, elementData, size, numNew);
         size += numNew;
         return numNew != 0;
    }

    @Override
    public boolean addAll(int index, Collection c) {
        Object[] a = c.toArray();
        int numNew = a.length;
        elementData = Arrays.copyOf(elementData, + numNew);
        int numMoved = size - index;
        if (numMoved > 0) {
            System.arraycopy(elementData, index, elementData, index + numNew, numMoved);
        }
     System.arraycopy(a, 0, elementData, index, numNew);
     size += numNew;
     return numNew != 0;
    }



    @Override
    public void clear() {
        for (int i = 0; i < size; i++)
          elementData[i] = null;

    size = 0;
    }

    @Override
    public Object get(int index) {
        return  elementData[index];
    }


    @Override
    public Object set(int index, Object element) {
                 Object b = elementData(index);
                  elementData[index] = element;
                 return b;

    }

    private Object elementData(int index) {
        return (Object) elementData[index];
    }


    @Override
    public Object remove(int index) {
        if (index < size){
           int numMoved = size - index - 1;
             if (numMoved > 0)
            System.arraycopy(elementData, index+1, elementData, index, numMoved);
             elementData[--size] = null;
           return true;
        }
           return false;
    }


    @Override
    public int indexOf(Object o) {
        for (int i = 0; i < size; i++){
         if (o.equals(elementData[i]))
          return i;}
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        for (int i = size-1; i >= 0; i--){
      if (o.equals(elementData[i]))
        return i;
           }
     return -1;
    }



    @Override
    public List subList(int fromIndex, int toIndex) {
        List Sublist = null;
        if (fromIndex < 0) {
            return Sublist;
        }else {
            if (toIndex > size) {
                return Sublist;
            }else {
                if (fromIndex > toIndex) {
                    return Sublist;
                }
            }
        }
        for(int i = fromIndex;i <= toIndex; i++){
            Sublist.add(elementData[i]);
        }

      return Sublist;
    }
    @Override
    public void replaceAll(UnaryOperator operator) {

    }

    @Override
    public boolean retainAll(Collection c) {
        Object[] a = c.toArray();
        int numNew = a.length;
        boolean k = false;
        for (int i= 0; i<size; i++) {
            boolean b = false;
            for (int index = 0; index < numNew; index++) {
                if (a[index].equals(elementData[i])) {
                    b = true;
                }
            }
         if(b!=true)   remove(i) ;
            k = true;
        }
        return k;
    }

    @Override
    public boolean removeAll(Collection c) {
        Object[] a = c.toArray();
        int numNew = a.length;
        boolean k = false;
        for(int i=0;i<numNew;i++){
         boolean n = remove(a[i]);
            if (n == true) {
               k = true;
            }
        }
        return k;
    }

    @Override
    public void sort(Comparator c) {

    }



    @Override
    public Spliterator spliterator() {
        return null;
    }

    @Override
    public boolean containsAll(Collection c) {
            Object[] a = c.toArray();
            int numNew = a.length;
            boolean k = false;
            for(int i=0;i<numNew;i++){
                boolean n = contains(a[i]);
                if (n == true) {
                    k = true;
                }
            }
            return k;
        }


    @Override
    public Object[] toArray(Object[] a) {
     if (a.length < size)
     return (Object[] ) Arrays.copyOf(elementData, size, a.getClass());
    System.arraycopy(elementData, 0, a, 0, size);
     if (a.length > size)
        a[size] = null;
     return a;
       }

    @Override
    public ListIterator listIterator() {
        return null;
    }


    @Override
    public ListIterator listIterator(int index) {
        return null;
    }



    @Override
    public Iterator iterator() {
        return null;
    }


}

