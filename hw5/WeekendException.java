package hw5;

import java.text.SimpleDateFormat;
import java.util.Date;

public class WeekendException extends Exception {
    public WeekendException(Date d) {
        super(new SimpleDateFormat("dd-MMMM-yyyy").format(d) + " - Weekend");
//        SimpleDateFormat format1 = new SimpleDateFormat("dd-MMMM-yyyy");
//        System.out.println(format1.format(d) + " - Weekend");
    }
}
