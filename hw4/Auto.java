package hw4;

abstract class Auto {
    protected double Vmax;
    protected double man;
    protected double acc;
    protected double Vcurrent;
    protected double Vo;
    protected double time;
    public static final double distance = 2000;


    public static void main(String[] args) {
    }
        public Auto(double Vmax, double man, double acc) {
        this.Vmax = Vmax;
        this.man = man;
        this.acc = acc;
    }

    public Auto (double Vmax,double man, double acc,double Vcur,double time){
        this(Vmax,man,acc);
        this.Vcurrent = Vcur;
        this.time = time;
    }

    public double getVmax() {
        return Vmax;
    }

    public void setVmax(double vmax) {
        Vmax = vmax;
    }

    public double getMan() {
        return man;
    }

    public void setMan(double man) {
        this.man = man;
    }

    public double getAcc() {
        return acc;
    }

    public void setAcc(double acc) {
        this.acc = acc;
    }

    public double getVcur() {
        return Vcurrent;
    }

    public void setVcur(double vcur) {
        Vcurrent = vcur;
    }

    public double getDist() {
        return distance;
    }

    public double getVo() {
        return Vo;
    }

    public void setVo(double vo) {
        Vo = vo;
    }

    public double getTime() {
        return time;
    }

    public void setTime(double time) {
        this.time = time;
    }


    public void move() {
        int distance1;
        Vcurrent = (int) Math.sqrt(2 * distance * acc + Math.pow(Vcurrent, 2));
        if (Vcurrent > Vmax) {
            distance1 = (int) ((Math.pow(Vmax, 2) - Math.pow(Vo, 2)) / (2 * acc));
            time += (Vmax - Vo) / acc;
            time += ((distance - distance1) / Vmax);
        } else {
            time += (Vcurrent - Vo) / acc;
        }

    }

    public void conversion() {
        Vcurrent = Vcurrent * 1000 / 3600;
    }

    public abstract void turn();
}
