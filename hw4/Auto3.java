package hw4;

public class Auto3 extends Auto {

    Auto3(double Vmax, double man, double acc) {
        super(Vmax, man, acc);
    }
    @Override
    public void turn() {
        if (Vcurrent >= Vmax) {
            Vmax += Vmax * 1.1;
        }
        Vo = Vcurrent * man;
    }
}