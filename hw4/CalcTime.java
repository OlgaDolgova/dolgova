package hw4;

public class CalcTime {
    public static void main(String args[]) {
        Auto[] Arr = new Auto[3];
        Arr[0] = new Auto1(170, 0.5, 10);
        Arr[1] = new Auto2(150, 0.4, 30);
        Arr[2] = new Auto3(190, 0.6, 40);

        for (int i = 0; i < 3; i++) {
            Arr[i].conversion();
            for (int j = 0; j < 20; j++) {
                Arr[i].move();
                Arr[i].turn();
            }
        }

        for (int i = Arr.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (Arr[j].time > Arr[j + 1].time) {
                    double tmp = Arr[j].time;
                    Arr[j].time = Arr[j + 1].time;
                    Arr[j + 1].time = tmp;
                }
            }
        }

        for (int j = 0; j < 3; j++) {

            System.out.println("Автомобиль " + (j+1) + " прошел трассу за " + (int) (Arr[j].time / 60) + " минут(ы) " + (int) (Arr[j].time % 60) + " секунд(ы)");

        }
    }
}
