package hw4;

public class Auto2 extends Auto {

    Auto2(double Vmax, double man, double acc) {
        super(Vmax, man, acc);
    }

    @Override
    public void turn() {
        if (Vo < Vmax / 2) {
            Vcurrent = Math.sqrt((4 * distance * acc) + Math.pow(Vcurrent, 2));
            Vo = Vcurrent * man;
        } else {
            Vo = Vcurrent * man;
        }
    }
}
