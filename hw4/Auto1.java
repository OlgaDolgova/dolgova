package hw4;

public class Auto1 extends Auto {

    Auto1(double Vmax, double man, double acc) {
        super(Vmax, man, acc);
    }
    @Override
    public void turn() {
        if (Vcurrent > Vmax / 2) {
            Vo = (man + ((Vmax / 2 - Vcurrent) * 0.005)) * Vcurrent;
        } else {
            Vo = Vcurrent * man;
        }
    }
}